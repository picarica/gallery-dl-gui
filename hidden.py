import os
import glob

home_directory = os.environ.get("HOME")
extractor_path = os.path.join(home_directory, ".local/lib/python3.10/site-packages/gallery_dl/extractor")
modules = glob.glob(os.path.join(extractor_path, "*.py"))
hidden_imports = [f"gallery_dl.extractor.{os.path.splitext(os.path.basename(module))[0]}" for module in modules]

with open("hidden_imports.txt", "w") as f:
    f.write(" ".join(f"--hidden-import={module}" for module in hidden_imports))