import tkinter as tk
import threading
import subprocess
import multiprocessing
import gallery_dl
import os
import importlib.resources
import certifi
from tkinter import messagebox

with importlib.resources.path("certifi", "cacert.pem") as ca_path:
    os.environ["REQUESTS_CA_BUNDLE"] = str(ca_path)
URLS_FILE = "urls.txt"

class DownloadManager:
    def __init__(self, command, app):
        self.command = command
        self.jobs = []
        self.app = app
        self.is_downloading = False
        self.stop_requested = False

    def start_download(self):
        self.app.set_status("Downloading...")
        self.is_downloading = True
        self.process = multiprocessing.Process(target=self.download_process, args=(self.command,)) 
        self.process.start()  

    def download_process(self, command):  
        for url in command[1:]:
            job = gallery_dl.job.DownloadJob(url)
            job.run()
            self.jobs.append(job)
        self.is_downloading = False
        self.app.set_status("Download finished.")

    def stop_download(self):
        if self.process and self.process.is_alive():
            self.process.terminate()
            self.process.join()
            self.is_downloading = False
            self.app.set_status("Download stopped.")


class App:
    def __init__(self, master):
        self.master = master
        self.urls_text = tk.Text(master, height=10, width=50)
        self.urls_text.pack()

        # Add the checkboxes
        checkbox_frame = tk.Frame(master)
        checkbox_frame.pack()

        image_description = tk.Label(checkbox_frame, text="\nBy checking any of these checkboxes it will only download selected option\n if none is selected all is downloaded\n")
        image_description.pack()
        self.image_var = tk.BooleanVar()
        self.image_checkbox = tk.Checkbutton(checkbox_frame, text="Images", variable=self.image_var)
        self.image_checkbox.pack(side=tk.LEFT)

        
        self.video_var = tk.BooleanVar()
        self.video_checkbox = tk.Checkbutton(checkbox_frame, text="Videos", variable=self.video_var)
        self.video_checkbox.pack(side=tk.LEFT)

        buttons_frame = tk.Frame(master)
        buttons_frame.pack()

        self.start_button = tk.Button(buttons_frame, text="Start Download", command=self.start_download)
        self.stop_button = tk.Button(buttons_frame, text="Stop Download", command=self.stop_download)
        self.start_button.pack(side=tk.LEFT)
        self.stop_button.pack(side=tk.LEFT)

        self.status_label = tk.Label(master, text="Ready", bd=1, relief=tk.SUNKEN, anchor=tk.W)
        self.status_label.pack()

        self.download_manager = None
        self.load_urls()

    def load_urls(self):
        try:
            with open(URLS_FILE, "r") as file:
                urls = file.readlines()
                urls = [url.strip() for url in urls if not url.startswith("#")]
                self.urls_text.insert("1.0", "\n".join(urls))
        except FileNotFoundError:
            pass

    def save_urls(self):
        urls = self.urls_text.get("1.0", "end").strip()
        with open(URLS_FILE, "w") as file:
            file.write(urls)

    def start_download(self):
        urls = self.urls_text.get("1.0", "end").strip().split("\n")
        urls = [url for url in urls]

        # Check the state of the checkboxes
        image_only = self.image_var.get()
        video_only = self.video_var.get()

        # Prepare the command with appropriate filters
        command = ["gallery-dl"]
        if image_only:
            command.extend(["--filter", "extension in ('jpg', 'png', 'jpe', 'zip', 'gif')"])
        elif video_only:
            command.extend(["--filter", "extension in ('mp4', 'm4v', 'webm', 'avi')"])
        command.extend(urls)

        self.download_manager = DownloadManager(command, self)
        threading.Thread(target=self.download_manager.start_download).start()
        self.save_urls()

    def stop_download(self):
        if self.download_manager:
            self.download_manager.stop_download()

    def set_status(self, status):
        self.status_label.config(text=status)

    def select_all(self, event):
        self.urls_text.tag_add("sel", "1.0", "end")

    def on_closing(self):
        if self.download_manager and self.download_manager.is_downloading:
            result = messagebox.askyesno("Confirm", "Are you sure you want to exit? The download is still in progress.")
            if not result:
                return
            self.stop_download()
        self.save_urls()
        self.master.destroy()

root = tk.Tk()
app = App(root)
root.protocol("WM_DELETE_WINDOW", app.on_closing)
root.mainloop()
